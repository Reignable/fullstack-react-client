const proxy = require('http-proxy-middleware');
/* eslint-disable */
module.exports = function(app) {
  app.use(
    proxy('/auth/google', {
      target: 'http://server:5000/' // server is docker-compose stuff
    })
  );
  app.use(
    proxy('/api/*', {
      target: 'http://server:5000'
    })
  );
};
