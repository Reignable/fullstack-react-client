import React from 'react';
import PropTypes from 'prop-types';
import StripeCheckout from 'react-stripe-checkout';
import { connect } from 'react-redux';
import * as actions from '../../actions';

const StripeCheckoutWrapper = ({ handleToken }) => (
  <StripeCheckout
    name="Emaily"
    description="Add credits to your account"
    amount={500}
    token={token => handleToken(token)}
    stripeKey={process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY}
  >
    <button className="btn" type="submit">
      Add Credits
    </button>
  </StripeCheckout>
);

StripeCheckoutWrapper.propTypes = {
  handleToken: PropTypes.func.isRequired
};

export default connect(
  null,
  actions
)(StripeCheckoutWrapper);
