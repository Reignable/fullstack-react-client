FROM node:alpine

ENV NPM_CONFIG_LOGLEVEL warn

RUN mkdir -p /client
WORKDIR /client
COPY . .

RUN npm install

CMD npm start

EXPOSE 3000
